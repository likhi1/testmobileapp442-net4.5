﻿/// <reference path="../angular.js" />
/// <reference path="Module.js" />


app.service('crudService', function ($http) {
    var BasePath = "http://localhost:1185";

    //Create new record
    this.post = function (Employee) {
        var request = $http({
            method: "post", 
            url: BasePath + "/api/EmployeesAPI",
            data:  Employee
        });
        return request;
    }
    //Get Single Records
    this.get = function (EmpNo) {
        return $http.get(BasePath + "/api/EmployeesAPI/" + EmpNo);
    }

    //Get All Employees
    this.getEmployees = function () { 
        return $http.get(BasePath + "/api/EmployeesAPI");
    }


    //Update the Record
    this.put = function (EmpNo, Employee) {
        var request = $http({
            method: "put",
            url: BasePath + "/api/EmployeesAPI/" + EmpNo,
            data: Employee
        });
        return request;
    }
    //Delete the Record
    this.delete = function (EmpNo) {
        var request = $http({
            method: "delete",
            url: BasePath + "/api/EmployeesAPI/" + EmpNo
        });
        return request;
    }

});
